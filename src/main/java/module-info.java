module de.kks.kugelcontroll {
    requires javafx.controls;
    requires javafx.fxml;

    requires javafx.graphics;
    requires javafx.base;
    requires com.google.gson;
    requires kugelmatik.java.api;

    opens de.kks.kugelcontrol to javafx.fxml;
    exports de.kks.kugelcontrol;
    exports de.kks.kugelcontrol.kugelmatik;
}
