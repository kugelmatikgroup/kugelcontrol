package de.kks.kugelcontrol.kugelmatik;

import de.kks.kugelcontrol.utils.IDisplayable;
import de.kks.kugelcontrol.utils.IKugelmatikControlls;
import de.kks.kugelcontrol.utils.IKugelmatikCoordinates;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import de.kks.api.Coordinate;
import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;

import java.io.IOException;

public class Cluster extends Button implements IKugelmatikControlls, IKugelmatikCoordinates, IDisplayable {

    private final int x;
    private final int y;

    private final Stepper[] steppers;

    public Cluster(int x, int y) {
        super();
        this.x = x;
        this.y = y;
        this.getStylesheets().add(String.valueOf(getClass().getResource("/cluster.css")));
        this.getStyleClass().add("cluster");

        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        VBox vBox = new VBox();
        vBox.setFillWidth(true);
        Label coordinates = new Label(String.format("[%d,%d]", getX(), getY()));
        vBox.setSpacing(5);
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        steppers = new Stepper[Config.instance.getClusterWidth() * Config.instance.getClusterHeight()];

        for (int cx = 0; cx < Config.instance.getClusterWidth(); cx++) {
            for (int cy = 0; cy < Config.instance.getClusterHeight(); cy++) {
                steppers[cy * (Config.instance.getClusterHeight() - 1) + cx] = new Stepper(this, cx, cy);
                gridPane.addRow(Config.instance.getClusterHeight() - cy - 1, getStepperByRawPosition(cx, cy));
            }
            gridPane.addColumn(cx);
        }

        for (ColumnConstraints constraints : gridPane.getColumnConstraints()) {
            constraints.setMinWidth(100);
            constraints.setPrefWidth(100);
            constraints.setFillWidth(true);
        }
        for (RowConstraints constraints : gridPane.getRowConstraints()) {
            constraints.setMinHeight(25);
            constraints.setPrefHeight(25);
            constraints.setFillHeight(true);
        }

        vBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        vBox.getChildren().addAll(coordinates, gridPane);
        this.setGraphic(vBox);
        this.setOnAction(actionEvent -> {
            try {
                new ClusterWindow(this);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
    }

    @Override
    public void set(int height) throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.setCluster(height, new Coordinate(getX(),getY()));

        setLabel(height);
    }

    @Override
    public void stop() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.stopCluster(new Coordinate(getX(),getY()));
    }

    @Override
    public void fix() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.fixKugel(new Coordinate(getX(),getY()));
    }

    @Override
    public void home() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.sendHome(new Coordinate(getX(),getY()));

        setLabel(0);
    }

    @Override
    public int getX() {
        return x + 1;
    }

    @Override
    public int getY() {
        return y + 1;
    }

    @Override
    public int getRawX() {
        return x;
    }

    @Override
    public int getRawY() {
        return y;
    }

    public Stepper getStepperByPosition(int x, int y) {
        return getStepperByRawPosition(x - 1, y - 1);
    }

    public Stepper getStepperByRawPosition(int x, int y) {
        return steppers[y * (Config.instance.getClusterHeight() - 1) + x];
    }

    @Override
    public void setLabel(int height) {
        for (Stepper stepper : steppers) {
            stepper.setLabel(height);
        }
    }
}
