package de.kks.kugelcontrol.kugelmatik;

import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;
import de.kks.kugelcontrol.utils.UtilityFunctions;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;

public class ClusterWindow extends BorderPane {

    private final Cluster cluster;

    private VBox controlls;

    public ClusterWindow(Cluster cluster) throws IOException {
        super();
        this.cluster = cluster;

        Label WIP = new Label("Currently not available!");
        WIP.setAlignment(Pos.CENTER);
        WIP.setDisable(true);
        this.setLeft(WIP);

        GridPane gridPane = new GridPane();

        for (int x = 0; x < Config.instance.getClusterWidth(); x++) {
            for (int y = 0; y < Config.instance.getClusterHeight(); y++) {
                gridPane.addRow(Config.instance.getClusterHeight() - y - 1, getCell(x, y));
            }
            gridPane.addColumn(x);
        }
        gridPane.setPadding(new Insets(10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        this.setCenter(gridPane);

        Slider slider = new Slider();
        slider.setMin(0);
        slider.setMax(Config.instance.getMaxHeight());
        slider.setMajorTickUnit(1000);
        slider.setMinorTickCount(250);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setPadding(new Insets(5,10,5,10));
        slider.valueChangingProperty().addListener((observableValue, wasChanging, changing) -> {
            if(!changing) {
                try {
                    cluster.set((int) slider.getValue());
                } catch (UnregisteredCommandException | InvalidArgumentException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        this.setBottom(slider);

        controlls = new VBox();

        Button home = new Button("Send Cluster Home");
        home.setOnAction(actionEvent -> {
            try {
                cluster.home();
                slider.setValue(0);
            } catch (UnregisteredCommandException | InvalidArgumentException e) {
                throw new RuntimeException(e);
            }
        });
        Button stop = new Button("Stop Cluster");
        stop.setOnAction(actionEvent -> {
            try {
                cluster.stop();
            } catch (UnregisteredCommandException | InvalidArgumentException e) {
                throw new RuntimeException(e);
            }
        });

        controlls.getChildren().addAll(home,stop, UtilityFunctions.createSpacer(),new Pane());
        this.setRight(controlls);

        Stage stage = new Stage();
        stage.setScene(new Scene(this));
        stage.setTitle(String.format("Cluster [%d,%d]", cluster.getX(), cluster.getY()));
        stage.show();
    }

    private VBox getCell(int x, int y) {
        VBox vbox = new VBox();
        Stepper stepper = cluster.getStepperByRawPosition(x, y);
        Label label = stepper.copy();
        Slider slider = new Slider();
        slider.setMin(0);
        slider.setMax(Config.instance.getMaxHeight());
        slider.setMaxWidth(50);
        slider.valueChangingProperty().addListener((observableValue, wasChanging, changing) -> {
            if(!changing) {
                try {
                    stepper.set((int) slider.getValue());

                    controlls.getChildren().set(3,getStepperControlls(slider, stepper));
                } catch (UnregisteredCommandException | InvalidArgumentException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        vbox.getChildren().addAll(label, slider);
        return vbox;
    }

    public VBox getStepperControlls(Slider slider, Stepper stepper) {
        VBox stepperControlls = new VBox();

        Label stepperLabel = new Label(String.format("Stepper [%d,%d]", stepper.getX(), stepper.getY()));
        stepperLabel.setFont(Font.font(18));

        Button home = new Button("Send Stepper Home");
        home.setOnAction(actionEvent -> {
            try {
                stepper.home();
                slider.setValue(0);
            } catch (UnregisteredCommandException | InvalidArgumentException e) {
                throw new RuntimeException(e);
            }
        });
        Button stop = new Button("Stop Stepper");
        stop.setOnAction(actionEvent -> {
            try {
                stepper.stop();
            } catch (UnregisteredCommandException | InvalidArgumentException e) {
                throw new RuntimeException(e);
            }
        });
        Button fix = new Button("Fix Stepper");
        fix.setOnAction(actionEvent -> {
            try {
                stepper.fix();
            } catch (UnregisteredCommandException | InvalidArgumentException e) {
                throw new RuntimeException(e);
            }
        });

        stepperControlls.getChildren().addAll(stepperLabel, home, stop, fix, getTextField(slider, stepper));
        return stepperControlls;
    }

    private static TextField getTextField(Slider slider, Stepper stepper) {
        TextField textField = new TextField();
        textField.setPromptText("Set Stepper Height");
        textField.setOnAction(actionEvent -> {
            String text = textField.getText();

            try {
                int value = Integer.parseInt(text);
                stepper.set(value);
                slider.setValue(value);
            } catch (NumberFormatException e) {
                try {
                    stepper.set(0);
                    slider.setValue(0);
                } catch (UnregisteredCommandException | InvalidArgumentException ex) {
                    throw new RuntimeException(ex);
                }
            } catch (UnregisteredCommandException | InvalidArgumentException e) {
                throw new RuntimeException(e);
            } catch (IndexOutOfBoundsException ignored) {
            }
        });
        return textField;
    }

}
