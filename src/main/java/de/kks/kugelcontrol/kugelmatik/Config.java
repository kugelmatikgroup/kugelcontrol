package de.kks.kugelcontrol.kugelmatik;

public class Config {

    public static Config instance;
    private boolean darkMode;
    private int kugelmatikWidth;
    private int kugelmatikHeight;
    private int clusterWidth;
    private int clusterHeight;
    private int MAX_HEIGHT;

    public Config() {
        if (instance == null) {
            instance = this;
        }
        darkMode = true;
        kugelmatikWidth = 5;
        kugelmatikHeight = 5;
        clusterWidth = 5;
        clusterHeight = 6;
        MAX_HEIGHT = 8000;
    }

    public boolean isDarkMode() {
        return darkMode;
    }

    public void setDarkMode(boolean darkMode) {
        this.darkMode = darkMode;
    }

    public int getKugelmatikWidth() {
        return kugelmatikWidth;
    }

    public void setKugelmatikWidth(int kugelmatikWidth) {
        this.kugelmatikWidth = kugelmatikWidth;
    }

    public int getKugelmatikHeight() {
        return kugelmatikHeight;
    }

    public void setKugelmatikHeight(int kugelmatikHeight) {
        this.kugelmatikHeight = kugelmatikHeight;
    }

    public int getClusterWidth() {
        return clusterWidth;
    }

    public void setClusterWidth(int clusterWidth) {
        this.clusterWidth = clusterWidth;
    }

    public int getClusterHeight() {
        return clusterHeight;
    }

    public void setClusterHeight(int clusterHeight) {
        this.clusterHeight = clusterHeight;
    }

    public int getMaxHeight() {
        return MAX_HEIGHT;
    }

    public void setMaxHeight(int MAX_HEIGHT) {
        this.MAX_HEIGHT = MAX_HEIGHT;
    }
}
