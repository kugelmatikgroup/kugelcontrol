package de.kks.kugelcontrol.kugelmatik;

import de.kks.kugelcontrol.utils.IDisplayable;
import de.kks.kugelcontrol.utils.IKugelmatikControlls;
import de.kks.kugelcontrol.utils.UtilityFunctions;
import javafx.geometry.Insets;
import javafx.scene.control.Slider;
import javafx.scene.layout.*;
import de.kks.api.ApiHandler;
import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;

import java.io.IOException;

public class Kugelmatik extends HBox implements IKugelmatikControlls, IDisplayable {

    private final de.kks.kugelcontrol.kugelmatik.Cluster[] clusters;

    public static ApiHandler APIHANDLER;

    public Kugelmatik() {
        super();

        new Config();

        try {
            APIHANDLER = new ApiHandler();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        VBox vBox = new VBox();

        GridPane gridPane = new GridPane();
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        clusters = new Cluster[Config.instance.getKugelmatikWidth() * Config.instance.getKugelmatikHeight()];

        for (int x = 0; x < Config.instance.getKugelmatikWidth(); x++) {
            for (int y = 0; y < Config.instance.getKugelmatikHeight(); y++) {
                clusters[y * Config.instance.getKugelmatikHeight() + x] = new Cluster(x, y);
                gridPane.addRow(Config.instance.getKugelmatikHeight() - y - 1, getClusterByRawPosition(x, y));
            }
            gridPane.addColumn(x);
        }
        for (ColumnConstraints constraints : gridPane.getColumnConstraints()) {
            constraints.setFillWidth(true);
        }
        for (RowConstraints constraints : gridPane.getRowConstraints()) {
            constraints.setFillHeight(true);
        }

        Slider slider = new Slider();
        slider.setMin(0);
        slider.setMax(Config.instance.getMaxHeight());
        slider.setMajorTickUnit(1000);
        slider.setMinorTickCount(250);
        slider.setShowTickMarks(true);
        slider.setShowTickLabels(true);
        slider.setPadding(new Insets(5,10,5,10));
        slider.valueChangingProperty().addListener((observableValue, wasChanging, changing) -> {
            if(!changing) {
                try {
                    set((int) slider.getValue());
                } catch (UnregisteredCommandException | InvalidArgumentException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        slider.setPadding(new Insets(5));
        this.setFillHeight(true);
        vBox.setFillWidth(true);
        vBox.getChildren().addAll(gridPane, slider);
        this.getChildren().addAll(vBox, UtilityFunctions.createSpacer());
    }

    @Override
    public void set(int height) throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.setAll(height);

        setLabel(height);
    }

    @Override
    public void stop() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.stopAll();
    }

    @Override
    public void fix() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.fixAll();
    }

    @Override
    public void home() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.sendAllHome();

        setLabel(0);
    }

    public void startChoreographyWithDefaultSettings(String choreo) throws UnregisteredCommandException, InvalidArgumentException {
        switch (choreo) {
            case "sine":
                APIHANDLER.playChoreography(choreo, 5);
            case "ripple":
                APIHANDLER.playChoreography(choreo, 10);
            default:
                APIHANDLER.playChoreography(choreo, 5);
        }
    }

    public Cluster getClusterByPosition(int x, int y) {
        return getClusterByRawPosition(x - 1, y - 1);
    }

    public Cluster getClusterByRawPosition(int x, int y) {
        return clusters[y * Config.instance.getKugelmatikHeight() + x];
    }

    @Override
    public void setLabel(int height) {
        for (Cluster cluster : clusters) {
            cluster.setLabel(height);
        }
    }
}
