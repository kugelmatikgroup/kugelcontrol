package de.kks.kugelcontrol.kugelmatik;

import de.kks.kugelcontrol.utils.IDisplayable;
import de.kks.kugelcontrol.utils.IKugelmatikControlls;
import de.kks.kugelcontrol.utils.IKugelmatikCoordinates;
import de.kks.kugelcontrol.utils.UtilityFunctions;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import de.kks.api.Coordinate;
import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;

public class Stepper extends Label implements IKugelmatikControlls, IKugelmatikCoordinates, IDisplayable {

    private final Cluster cluster;
    private final int x;
    private final int y;

    private int height;

    public Stepper(Cluster cluster, int x, int y) {
        super();
        this.cluster = cluster;
        this.x = x;
        this.y = y;
        this.setAlignment(Pos.CENTER);
        this.setMinWidth(35);
        this.setMinHeight(10);
        this.setText("0");
        this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        this.height = 0;
    }

    @Override
    public void set(int height) throws UnregisteredCommandException, InvalidArgumentException {

        if(!UtilityFunctions.isBetween(0, Config.instance.getMaxHeight(), height))
            throw new IndexOutOfBoundsException("Height is not in between 0 and " + Config.instance.getMaxHeight());

        this.height = height;

        Kugelmatik.APIHANDLER.setStepper(height, new Coordinate(cluster.getX(), cluster.getY(),getX(),getY()));

        setLabel(height);
    }

    @Override
    public void setLabel(int height) {
        this.setText(height + "");
    }

    @Override
    public void stop() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.stopStepper(new Coordinate(cluster.getX(), cluster.getY(),getX(),getY()));
    }

    @Override
    public void fix() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.fixKugel(new Coordinate(cluster.getX(), cluster.getY(),getX(),getY()));
    }

    @Override
    public void home() throws UnregisteredCommandException, InvalidArgumentException {
        Kugelmatik.APIHANDLER.sendHome(new Coordinate(cluster.getX(), cluster.getY(),getX(),getY()));

        setLabel(0);
    }

    @Override
    public int getX() {
        return x + 1;
    }

    @Override
    public int getY() {
        return y + 1;
    }

    @Override
    public int getRawX() {
        return x;
    }

    @Override
    public int getRawY() {
        return y;
    }

    public Label copy() {
        Label copy = new Label();
        copy.textProperty().bind(this.textProperty());
        copy.setMinWidth(35);
        copy.setMinHeight(10);
        copy.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        return copy;
    }
}
