package de.kks.kugelcontrol.utils;

import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;

public interface IKugelmatikControlls {

    void set(int height) throws UnregisteredCommandException, InvalidArgumentException;

    void stop() throws UnregisteredCommandException, InvalidArgumentException;

    void fix() throws UnregisteredCommandException, InvalidArgumentException;

    void home() throws UnregisteredCommandException, InvalidArgumentException, UnregisteredCommandException, InvalidArgumentException;
}
