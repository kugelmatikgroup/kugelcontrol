package de.kks.kugelcontrol.utils;

import de.kks.kugelcontrol.utils.script.Command;
import de.kks.kugelcontrol.utils.script.CommandNotFound;
import de.kks.kugelcontrol.utils.script.InvalidArguments;
import de.kks.kugelcontrol.utils.script.commands.Set;
import de.kks.kugelcontrol.utils.script.commands.Wait;
import javafx.scene.control.TextArea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class ScriptHandler implements Runnable{

    private ArrayList<String> lineBuffer;
    private TextArea textArea;
    public ScriptHandler(TextArea textArea) {
        Command.registerCommand(new Set());
        Command.registerCommand(new Wait());
        lineBuffer = new ArrayList<>();
        this.textArea = textArea;
    }
    public void compileAndRunScript(String content) {
        lineBuffer.addAll(List.of(content.split("\n")));
    }

    @Override
    public void run() {
        while(true) {
            if (lineBuffer.isEmpty()) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                continue;
            }
            Pattern pattern = Pattern.compile("[A-Za-z]+\\([^)]*\\)", Pattern.CASE_INSENSITIVE);
            boolean syntaxError = false;
            ArrayList<String> copiedBuffer = new ArrayList<>(lineBuffer);
            for (int i = 0; i < lineBuffer.size(); i++) {
                String line = lineBuffer.get(i);
                if (!pattern.matcher(line).matches()) {
                    if (line.isEmpty() || line.charAt(0) == '#') {
                        copiedBuffer.remove(i);
                        continue;
                    }
                    copiedBuffer.set(i, lineBuffer.get(i) + "\n# Syntax error! /\\");
                    syntaxError = true;
                    continue;
                }
                if(syntaxError) {
                    continue;
                }
                String argument = line.substring(line.indexOf("(") + 1, line.indexOf(")"));
                boolean singleArgument = argument.contains(",");
                String[] args = null;
                if (singleArgument) {
                    args = argument.split(",");
                }
                try {
                    Command.runCommand(line.substring(0, line.indexOf("(")), singleArgument ? args : new Object[]{argument});
                } catch (CommandNotFound e) {
                    textArea.setText(String.format("# %s%n%s%n", e.getExceptionMessage(), textArea.getText()));
                } catch (InvalidArguments e) {
                    textArea.setText(String.format("# %s%n%s%n", e.getExceptionMessage(), textArea.getText()));
                }
                copiedBuffer.remove(0);
            }
            lineBuffer = new ArrayList<>(copiedBuffer);
            if (syntaxError) {
                StringBuilder finalString = new StringBuilder();
                for (String line : lineBuffer) {
                    finalString.append(line).append("\n");
                }
                textArea.setText(finalString.toString());
            }
        }
    }
}
