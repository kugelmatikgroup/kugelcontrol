package de.kks.kugelcontrol.utils;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class UtilityFunctions {
    public static boolean isBetween(int minValue, int maxValue, int value) {
        return value >= minValue && value <= maxValue;
    }

    public static Pane createSpacer() {
        Pane spacer = new Pane();
        spacer.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        VBox.setVgrow(spacer, Priority.ALWAYS);
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }
}
