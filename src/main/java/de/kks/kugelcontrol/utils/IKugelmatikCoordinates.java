package de.kks.kugelcontrol.utils;

public interface IKugelmatikCoordinates {
    int getX();

    int getY();

    int getRawX();

    int getRawY();
}
