package de.kks.kugelcontrol.utils.script;

public class InvalidArguments extends Throwable {
    private final String exceptionMessage;

    public InvalidArguments(String arguments) {
        super();
        this.exceptionMessage = arguments;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
