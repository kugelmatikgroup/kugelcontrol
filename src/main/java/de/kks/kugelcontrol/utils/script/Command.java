package de.kks.kugelcontrol.utils.script;

import java.util.ArrayList;

public abstract class Command {
    private static ArrayList<Command> registeredCommands;
    protected String name;
    protected String description;

    public Command(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public static void registerCommand(Command cmd) {
        if (registeredCommands == null) {
            registeredCommands = new ArrayList<>();
        } else if (registeredCommands.contains(cmd)) {
            return;
        }
        registeredCommands.add(cmd);
    }

    public static void runCommand(String cmd, Object... args) throws CommandNotFound, InvalidArguments {
        for (Command registeredCommand : registeredCommands) {
            if (!registeredCommand.name.equalsIgnoreCase(cmd)) {
                continue;
            }
            registeredCommand.run(args);
            return;
        }
        throw new CommandNotFound(cmd);
    }

    public abstract void run(Object... args) throws InvalidArguments;
}
