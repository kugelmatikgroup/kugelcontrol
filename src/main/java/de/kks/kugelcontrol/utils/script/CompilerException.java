package de.kks.kugelcontrol.utils.script;

public class CompilerException extends Throwable {
    private final String exceptionMessage;

    public CompilerException(String line) {
        super();
        exceptionMessage = line.replaceAll("\n", "\n#");
    }

    public CompilerException(String[] lines) {
        super();
        StringBuilder message = new StringBuilder();
        for (String line : lines) {
            message.append(line).append("\n");
        }
        exceptionMessage = message.toString();
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
