package de.kks.kugelcontrol.utils.script;

public class CommandNotFound extends Throwable {

    private final String exceptionMessage;

    public CommandNotFound(String commandName) {
        super();
        this.exceptionMessage = "There is no command under this name [" + commandName + "]";
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
