package de.kks.kugelcontrol.utils.script.commands;

import de.kks.kugelcontrol.utils.script.Command;
import de.kks.kugelcontrol.utils.script.InvalidArguments;

public class Wait extends Command {
    public Wait() {
        super("wait", "Wait for a specific duration");
    }

    @Override
    public void run(Object... args) throws InvalidArguments {
        if (args.length == 1) {
            synchronized (Thread.currentThread()){
                try{
                    Thread.currentThread().wait(Integer.parseInt(String.valueOf(args[0])));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            return;
        }
        throw new InvalidArguments("Please use following arguments\n# wait(<time in milliseconds>)");
    }
}
