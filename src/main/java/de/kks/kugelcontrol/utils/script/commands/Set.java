package de.kks.kugelcontrol.utils.script.commands;

import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;
import de.kks.kugelcontrol.MainController;
import de.kks.kugelcontrol.kugelmatik.Cluster;
import de.kks.kugelcontrol.utils.script.Command;
import de.kks.kugelcontrol.utils.script.InvalidArguments;
import javafx.application.Platform;

import java.util.Arrays;

public class Set extends Command {
    public Set() {
        super("set", "With this command you can set spheres to a certain height");
    }

    @Override
    public void run(Object... args) throws InvalidArguments {
        if (args.length == 1) {
            try {
                Platform.runLater(() -> {
                    try {
                        MainController.kugelmatik.set(Integer.parseInt(String.valueOf(args[0])));
                    } catch (UnregisteredCommandException e) {
                        throw new RuntimeException(e);
                    } catch (InvalidArgumentException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (NumberFormatException e) {
                throw new InvalidArguments(String.format("Argument '%s' is not a number!",args[0]));
            }
            return;
        } else if (args.length == 3) {
            try {
                Platform.runLater(() -> {
                    try {
                        Cluster cluster = MainController.kugelmatik.getClusterByPosition(Integer.parseInt(String.valueOf(args[0])), Integer.parseInt(String.valueOf(args[1])));
                        cluster.set(Integer.parseInt(String.valueOf(args[2])));
                    } catch (UnregisteredCommandException e) {
                        throw new RuntimeException(e);
                    } catch (InvalidArgumentException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (NumberFormatException e) {
                throw new InvalidArguments("Please check that all arguments are numbers!");
            }
            return;
        } else if (args.length == 5) {
            try {
                Platform.runLater(() -> {
                    try {
                        MainController.kugelmatik.getClusterByPosition(Integer.parseInt(String.valueOf(args[0])), Integer.parseInt(String.valueOf(args[1]))).getStepperByPosition(Integer.parseInt(String.valueOf(args[2])), Integer.parseInt(String.valueOf(args[3]))).set(Integer.parseInt(String.valueOf(args[4])));
                    } catch (UnregisteredCommandException e) {
                        throw new RuntimeException(e);
                    } catch (InvalidArgumentException e) {
                        throw new RuntimeException(e);
                    }
                });
            } catch (NumberFormatException e) {
                throw new InvalidArguments("Please check that all arguments are numbers!");
            }
            return;
        }
        throw new InvalidArguments("Please use following arguments\n# set(<height>)\n# set(<cluster-X>, <cluster-Y>, <height>)\n# set(<cluster-X>, <cluster-Y>, <stepper-X>, <stepper-Y>, <height>)");
    }
}
