package de.kks.kugelcontrol;

import de.kks.kugelcontrol.kugelmatik.Kugelmatik;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;

public class Main extends Application {

    protected static Stage stage;
    protected static HostServices hostServices;

    public static void main(String[] args) {
        launch(args);
        
    }

    @Override
    public void start(Stage stage) throws Exception {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if(Kugelmatik.APIHANDLER != null) {
                Kugelmatik.APIHANDLER.close();
            }
        }));

        Main.stage = stage;
        hostServices = getHostServices();
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/static.fxml")));
        stage.setScene(new Scene(root));
        stage.setTitle("KugelControl - v1.0-SNAPSHOT");
        stage.show();
    }
}
