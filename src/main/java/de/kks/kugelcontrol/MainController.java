package de.kks.kugelcontrol;

import de.kks.api.exceptions.InvalidArgumentException;
import de.kks.api.exceptions.UnregisteredCommandException;
import de.kks.kugelcontrol.kugelmatik.Config;
import de.kks.kugelcontrol.kugelmatik.Kugelmatik;
import de.kks.kugelcontrol.utils.ScriptHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    public static Kugelmatik kugelmatik;
    @FXML
    private BorderPane main;

    private ScriptHandler scriptHandler;
    @FXML
    void moveAllToZero(ActionEvent event) {
        try {
            kugelmatik.set(0);
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void moveAllDown(ActionEvent event) {
        try {
            kugelmatik.set(Config.instance.getMaxHeight());
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void sendAllHome(ActionEvent event) {
        try {
            kugelmatik.home();
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void stopAll(ActionEvent event) {
        try {
            kugelmatik.stop();
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void showApplicationInfos(ActionEvent event) {
        showInfoWindow();
    }

    @FXML
    void startChoreographyDistance(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("distance");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void startChoreographyLinear(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("linear");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void startChoreographyPlane(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("plane");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void startChoreographyRipple(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("ripple");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void startChoreographyShow(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("show");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void startChoreographySineWave(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("sine");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void startChoreographySplitPlane(ActionEvent event) {
        try {
            kugelmatik.startChoreographyWithDefaultSettings("splitplane");
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void stopCurrentChoreography(ActionEvent event) {
        try {
            kugelmatik.stop();
        } catch (UnregisteredCommandException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    void toggleAutoChoreographyStop(ActionEvent event) {

    }

    @FXML
    void choreographyScript(ActionEvent event) {
        showScriptWindow();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        kugelmatik = new Kugelmatik();
        kugelmatik.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        scriptHandler = null;
        main.setCenter(kugelmatik);
        Main.stage.centerOnScreen();
        Main.stage.sizeToScene();
        Main.stage.setResizable(false);
    }

    private void showInfoWindow() {
        Stage stage = new Stage();
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,10,10,10));
        vBox.setSpacing(15);

        Label title = new Label(Main.stage.getTitle());
        title.setFont(Font.font(36));

        Label repo = new Label("Contribution");
        repo.setFont(Font.font(24));

        Label repoText = new Label("This software was written in Java by the KugelmatikTeam, if you want to contribute please visit our GitLab Repository");
        Hyperlink hyperlink = new Hyperlink("https://gitlab.com/kugelmatikgroup/kugelcontrol");
        hyperlink.setOnAction(actionEvent -> Main.hostServices.showDocument(hyperlink.getText()));

        vBox.getChildren().addAll(title, repo, repoText, hyperlink);

        stage.setScene(new Scene(vBox));
        stage.setTitle(Main.stage.getTitle());
        stage.show();
    }

    private void showScriptWindow() {

        Stage stage = new Stage();
        stage.setMinHeight(600);
        stage.setMinWidth(800);
        stage.setResizable(false);
        VBox vBox = new VBox();
        vBox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        vBox.setFillWidth(true);
        TextArea textArea = new TextArea();
        textArea.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        if(scriptHandler == null) {
            scriptHandler = new ScriptHandler(textArea);
            new Thread(scriptHandler, "ScriptHandler").start();
        }
        Button runButton = new Button("Execute Script");
        runButton.setBackground(Background.fill(Color.ALICEBLUE));
        runButton.setMaxWidth(Double.MAX_VALUE);
        runButton.setOnAction(actionEvent -> {
            scriptHandler.compileAndRunScript(textArea.getText());
        });
        VBox.setVgrow(textArea, Priority.ALWAYS);
        vBox.getChildren().addAll(textArea, runButton);
        stage.setTitle("Kugelcontroll - ChoreographyScript");
        stage.setScene(new Scene(vBox));
        stage.show();
    }


}
